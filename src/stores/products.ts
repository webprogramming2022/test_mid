import { ref } from "vue";
import { defineStore } from "pinia";
import type ListProduct from "./types/listProduct";

export const useProductStore = defineStore("Product", () => {
  const listProduct = ref<ListProduct[]>([]);
  const sum = ref(0);
  const Product = ref<ListProduct>({
    id: -1,
    name: "",
    price: 0,
  });
  const clear = () => {
    Product.value = {
      id: -1,
      name: "",
      price: 0,
    };
  };
  const SumProducts = () => {
    sum.value = 0;
    for (let i = 0; i < listProduct.value.length; i++) {
      sum.value += listProduct.value[i].price;
    }
  };
  const Add = (menu: ListProduct) => {
    Product.value.id = menu.id;
    Product.value.name = menu.name;
    Product.value.price = menu.price;
    listProduct.value.push(Product.value);
    clear();
    SumProducts();
  };

  return { listProduct, Add, sum };
});
