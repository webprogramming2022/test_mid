export default interface ListProduct {
  id: number;
  name: string;
  price: number;
}
